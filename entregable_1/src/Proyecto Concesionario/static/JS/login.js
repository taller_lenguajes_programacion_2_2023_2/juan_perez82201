document.getElementById("loginForm").addEventListener("submit", function(event) {
    event.preventDefault();
    const username = document.getElementById("loginUsername").value;
    const password = document.getElementById("loginPassword").value;

    // Aquí puedes implementar la lógica de autenticación
    // Por ejemplo, verificar si el usuario y la contraseña coinciden

    // Si la autenticación es exitosa, almacena la información en el Local Storage
    localStorage.setItem("username", username);
    alert("Inicio de sesión exitoso.");
});
