document.getElementById("registerForm").addEventListener("submit", function(event) {
    event.preventDefault();
    const username = document.getElementById("registerUsername").value;
    const fullName = document.getElementById("registerFullName").value;
    const email = document.getElementById("registerEmail").value;
    const password = document.getElementById("registerPassword").value;
    const confirmPassword = document.getElementById("confirmPassword").value;

    // Validación de contraseñas iguales
    if (password !== confirmPassword) {
        alert("Las contraseñas no coinciden. Por favor, inténtalo de nuevo.");
        return;
    }

    // Validación de formato de correo electrónico
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!email.match(emailRegex)) {
        alert("Por favor, ingresa una dirección de correo electrónico válida.");
        return;
    }

    // Aquí puedes implementar la lógica de registro
    // Por ejemplo, verificar si el usuario ya existe

    // Si el registro es exitoso, almacena la información en el Local Storage
    localStorage.setItem("username", username);
    alert("Registro exitoso.");
});

// Redirigir al hacer clic en el botón "Volver"
document.getElementById("goBackButton").addEventListener("click", function() {
    // Cambiar la URL a la página a la que deseas redirigir
    window.location.href = "login.html";
});


